package com.yearup.todosapidemo.controller;

import com.yearup.todosapidemo.TodosApiDemoApplication;
import com.yearup.todosapidemo.domain.Todo;
import com.yearup.todosapidemo.dto.SuccessDetail;
import com.yearup.todosapidemo.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
public class TodoController {

    private static final Logger log = LoggerFactory.getLogger(TodosApiDemoApplication.class);

    @Autowired
    TodoService todoService;

    //TODO Get all todos using service
    @RequestMapping(method = RequestMethod.GET, value = "/todos")
    public ResponseEntity<List<Todo>> getAllTodos() {
        List<Todo> todos = todoService.getTodos();
        log.info("Get all Todos " + todos);
        SuccessDetail successDetail = new SuccessDetail(HttpStatus.OK.value(), "All todos retrieved successfully", todos);
        return new ResponseEntity(successDetail, HttpStatus.OK);
    }

    //TODO Get a todo by ID
    @RequestMapping(method = RequestMethod.GET, value = "/todos/{id}")
    public ResponseEntity<Optional<Todo>> getTodoById(@PathVariable Long id) {
        Optional<Todo> todo = todoService.getTodoById(id);
        log.info("Get todo by id: " + id + "\n" + todo);
        SuccessDetail successDetail = new SuccessDetail(HttpStatus.OK.value(), "Todo with id: " + id + " retrieved successfully", todo);
        return new ResponseEntity(successDetail, HttpStatus.OK);
    }

    //TODO Add a todo
    @RequestMapping(method = RequestMethod.POST, value = "todos")
    public ResponseEntity<?> addTodo(@Valid @RequestBody Todo todo) {
        Todo t = todoService.addTodo(todo);
        HttpHeaders httpHeaders = new HttpHeaders();
        URI newTodoUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/todos")
                .buildAndExpand(todo.getId())
                .toUri();
        httpHeaders.setLocation(newTodoUri);
        log.info("Created a new todo: " + t);
        SuccessDetail successDetail = new SuccessDetail(HttpStatus.CREATED.value(), "Todo successfully created", todo);
        return new ResponseEntity<>(successDetail, httpHeaders, HttpStatus.CREATED);
    }

    //TODO Update a todo by ID
    @RequestMapping(method = RequestMethod.PUT, value = "todos/{id}")
    public ResponseEntity<?> updateTodoById(@Valid @RequestBody Todo todo, @PathVariable Long id) {
        Todo t = todoService.updateTodoById(todo, id);
        log.info("Todo updated by id: " + id + "\n" + t);
        SuccessDetail successDetail = new SuccessDetail(HttpStatus.ACCEPTED.value(), "Todo successfully updated by id: " + id, todo);
        return new ResponseEntity<>(successDetail, HttpStatus.ACCEPTED);
    }

    //TODO Delete a todo by ID
    @RequestMapping(method = RequestMethod.DELETE, value = "todos/{id}")
    public ResponseEntity<?> deleteTodoById(@PathVariable Long id) {
        todoService.deleteTodoById(id);
        log.info("Todo deleted by id: " + id );
        SuccessDetail successDetail = new SuccessDetail(HttpStatus.NO_CONTENT.value(), "Todo successfully deleted by id: " + id);
        return new ResponseEntity<>(successDetail, HttpStatus.NO_CONTENT);
    }
}
