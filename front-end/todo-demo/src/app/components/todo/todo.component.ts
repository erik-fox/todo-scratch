import { Component, OnInit } from '@angular/core';
import { Todo } from '../todos/todos.component';
import { TodoDataService } from 'src/app/services/todo-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  id: number;

  todo: Todo;

  constructor(
    private serv: TodoDataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.todo = new Todo(this.id, '', false);

    if (this.id != -1) {
      this.serv.getTodoById(this.id).subscribe(
        data => this.todo = data
      );
    }
  }

  saveTodo() {
    if(this.id == -1) {
      this.serv.addTodo(this.todo).subscribe(
        data => {
          console.log(data)
          this.router.navigate(['todos'])
        }
      );
    } else {
      this.serv.updateTodoById(this.id, this.todo).subscribe(
        data => {
          console.log(data)
          this.router.navigate(['todos'])
        }
      );
    }
  }



}
