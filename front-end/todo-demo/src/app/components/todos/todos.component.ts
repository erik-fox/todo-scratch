import { Component, OnInit, ViewChild } from '@angular/core';
import { TodoDataService } from 'src/app/services/todo-data.service';
import { Router } from '@angular/router';
import { MatTable } from '@angular/material/table';

export class Todo{
  constructor(
    public id: number,
    public title: string,
    public completed: boolean
  ) {}
}

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  displayedColumns: string[] = ['id', 'title', 'done', 'update', 'delete'];

  dataSource = this.serv.getAllTodos();

  @ViewChild(MatTable) table: MatTable<Todo>;

  todos: Todo[];

  message: string;

  emptyError: string;

  constructor(
    private serv: TodoDataService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.refreshTodos();
  }

  refreshTodos(){
    this.serv.getAllTodos().subscribe(
      response => {
        console.log(response);
        this.todos = response;
      }
    )
  }

  addTodo() {
    this.router.navigate(['todo', -1]);
  }

  updateTodo(id) {
    console.log(`Updated Todo with id: ${id}`);
    this.router.navigate(['todo', id]);
  }
  
  deleteTodo(id) {
    console.log(`deleted todo with id: ${id}`)
    this.serv.deleteTodoById(id).subscribe(
      response => {
        console.log(response);
        this.message = `Successfully Deleted Todo #${id}`;
        this.refreshTodos();
        this.table.renderRows();
      }
    )
  }
}
