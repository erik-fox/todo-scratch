import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_URL } from '../app.constant';
import { Todo } from '../components/todos/todos.component';

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  constructor(private http: HttpClient) { }

  getAllTodos() {
    return this.http.get<Todo[]>(`${API_URL}/todos`);
  }

  getTodoById(id) {
    return this.http.get<Todo>(`${API_URL}/todos/${id}`);
  }

  addTodo(todo) {
    return this.http.post(`${API_URL}/todos`, todo);
  }

  updateTodoById(id, todo) {
    return this.http.put(`${API_URL}/todos/${id}`, todo);
  }

  deleteTodoById(id) {
    return this.http.delete(`${API_URL}/todos/${id}`);
  }
}
